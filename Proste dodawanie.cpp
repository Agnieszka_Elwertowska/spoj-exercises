//RNO_DOD - Proste dodawanie
//https://pl.spoj.com/problems/RNO_DOD/

#include <iostream>
using namespace std;

int t, n;
int liczba, wynik;

int main() 
{
	cin >> t;
	if (0 < t < 100)
	{
		for (int i = 0; i < t; i++) //lub while (t--)
		{
			cin >> n;
			wynik = 0;
			for (int i = 0; i < n; i++)
			{
				cin >> liczba;
				wynik = wynik + liczba; 
			}
		cout  << wynik << '\n';
		}
	}
	return 0;
}
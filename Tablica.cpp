//TABLICA - Tablica
//https://pl.spoj.com/problems/TABLICA/

#include <iostream>
using namespace std;

int tab[20];

int main()
{
	int licznik = 0;
	for (int i = 0; cin >> tab[i]; i++)
	{
		licznik++;
	}
	
	for (int i = licznik - 1; i >= 0; i--)
	{
		cout << tab[i] << " ";
	}
	return 0;
}
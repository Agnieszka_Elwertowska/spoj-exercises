//CALC - Kalkulator
//https://pl.spoj.com/problems/CALC/

#include <iostream>
using namespace std;
 
int main()
{
	int a,b;
	char znak;
	while (cin >> znak >> a >> b)
	{
		switch (znak)
		{
		case '+' :
			cout << a + b << '\n';
			break;
		case '-' :
			cout << a - b << '\n';
			break;
		case '*' :
			cout << a * b << '\n';
			break;
		case '/' :
			if (b != 0)
			{
				cout << a / b << '\n';
			}
			break;
		case '%' :
			cout << a % b << '\n';
			break;
		}
	}
	return 0;
}
//ROWNANIE - Równanie kwadratowe
//https://pl.spoj.com/problems/ROWNANIE/

#include <iostream>
#include <math.h>
using namespace std;

double a, b, c;
double delta;

int main() 
{
	while (cin >> a >> b >> c)
	{
		delta = (pow(b,2) - 4*a*c);
		
		if (sqrt(delta) > 0)
		{
			cout << "2" << '\n';
		}
		else if (sqrt(delta) == 0)
		{
			cout << "1" << '\n';
		}
		else
		{
			cout << "0" << '\n';
		}
	}
	return 0;
}
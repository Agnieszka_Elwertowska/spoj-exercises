//PTROL - ROL
//https://pl.spoj.com/problems/PTROL/

#include <iostream>
using namespace std;

int t, n;

int main() 
{
	cin >> t;
	if (t <= 100)
	{
		while (t--)
		{
			cin >> n;
			if (1 < n <= 100)
			{
				int * tab = new int [n];
				for (int i = 0; i < n; i++)
				{
					cin >> tab[i];
				}
				for (int i = 1; i < n; i++)
				{
					cout << tab[i] << " ";
				}
				cout << tab[0] << '\n';
			}
		}
	}
	return 0;
}
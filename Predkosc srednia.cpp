//VSR - Predkość średnia
//https://pl.spoj.com/problems/VSR/

#include <iostream>
using namespace std;

int t, v1, v2;

int main() 
{
	cin >> t;
	if (1 <= t <= 1000) 
	{
		for (int i=0; i < t; i++) //lub while (t--)
		{
			cin >> v1 >> v2;
			cout << (2*v1*v2/(v1+v2)) << '\n';
		}
	}
	return 0;
}
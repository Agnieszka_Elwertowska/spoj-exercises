//TRN - Transponowanie macierzy
//https://pl.spoj.com/problems/TRN/

#include <iostream>
using namespace std;

int wiersz, kolumna;

int main() 
{
	cin >> wiersz >> kolumna;
	
	int ** macierz = new int * [wiersz];
	for(int z = 0; z < wiersz; z++)
	{
		macierz[z] = new int [kolumna];
	}

	for (int i = 0; i < wiersz; i++)
	{
		for (int j = 0; j < kolumna; j++)
		{
			cin >> macierz[i][j];
		}
	}

	for (int i = 0; i < kolumna; i++)
	{
		for (int j = 0; j < wiersz; j++)
		{
			cout << " " << macierz[j][i];
		}
	}
	return 0;
} 
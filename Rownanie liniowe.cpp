//JROWLIN - Równanie liniowe
//https://pl.spoj.com/problems/JROWLIN/

#include <iostream>
using namespace std;

double a, b, c;

int main() 
{
	cin >> a >> b >> c;
	if (a != 0)
	{
		cout.precision(2);
		cout << fixed << ((c - b) / a) << '\n';
	}
	else // if (a == 0)
	{
		if (b != c)
		{
			cout << "BR" << '\n';
		}
		else // if (b == c)
		{
			cout << "NWR" << '\n';
		}
	}
	return 0;
}
//FLAMASTE - Flamaster
//https://pl.spoj.com/problems/FLAMASTE/

#include <iostream>
#include <string>
using namespace std;

int main()
{
    int c;
    cin >> c;
    if (1 <= c <= 50)
    {
    	while (c > 0) //lub while (c--)
    	{
        	string wyraz;
        	cin >> wyraz;
        	int licznik = 1;
        	for (int i = 0; i < wyraz.length(); i++) //lub (int i = 1; i <= wyraz.length(); i++)
        	{
            	if (wyraz[i+1] != wyraz[i]) //wtedy kazdy wyraz[i] zamiast wyraz[i+1] i wyraz[i-1] zamiast wyraz[i]
            	{
                	if(licznik > 2)
                	{
                		cout << wyraz[i] << licznik; //lub wyraz[i-1]
                	}
                	else if (licznik == 2)
                	{
                		cout << wyraz[i] << wyraz[i]; //lub wyraz[i-1]
                	}
                	else
                	{
                		cout << wyraz[i]; //lub wyraz[i-1]
                	}
                	licznik = 1;
            	}
            	else
            		licznik++;
        	}
    	cout << '\n'; //lub cout << "\n";
    	c--; //lub bez tego, gdy while (c--)
    	}
    }
    return 0;
}
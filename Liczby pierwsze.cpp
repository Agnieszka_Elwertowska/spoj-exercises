//PRIME_T - Liczby Pierwsze
//https://pl.spoj.com/problems/PRIME_T/

#include <iostream>
//#include <cstdlib>
using namespace std;

int n;
int liczba, reszta;
int dzielniki;

int main()
{
	cin >> n;
	if (n < 100000)
	{
		for (int i = 0; i < n; i++)
		{
			cin >> liczba;
			if (1 < liczba < 10000)
			{
				if (liczba < 2)
				{
					cout << "NIE" << '\n';
				}
				else if (liczba == 2)
				{
					cout << "TAK" << '\n';
				}
				else 
				{
					dzielniki = 0;
					for (int i = 2; i < liczba; i++)
					{
						reszta = liczba % i;
						if (reszta == 0)
						{
							dzielniki++;
						}
					}
				if (dzielniki > 0)
				{
					cout << "NIE" << '\n';
				}
				else
				{
					cout << "TAK" << '\n';
				}
				dzielniki = 0;
				}
			}
		}
	}
	//system("PAUSE");
	return 0;
}
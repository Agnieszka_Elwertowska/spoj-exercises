//PP0502B - Tablice
//https://pl.spoj.com/problems/PP0502B/

//#include <cstdlib>
#include <iostream>
using namespace std;

int t, n;
//int * tab = new int[n];
//int tab [100];

int main()
{
	cin >> t;
	for (int i = 0; i < t; i++)
	{
		cin >> n;
		int * tab = new int[n];
		for (int i = 0; i < n; i++)
		{
			cin >> tab[i];
		}
		for (int i = n - 1; i >= 0; i--)
		{
			cout << tab[i] << " ";
		}
		cout << '\n';
	}
	//system("PAUSE");
	return 0;
}
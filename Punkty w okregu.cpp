//PICIRC - Punkty w okręgu
//https://pl.spoj.com/problems/PICIRC/

#include <iostream>
#include <math.h>
using namespace std;

int xo, yo, ro, n;
int x, y, p;
int roo, xxo, yyo;

int main() 
{
	cin >> xo >> yo >> ro;
	cin >> n;
	roo = pow(ro, 2);
	for (int i = n; i > 0; i--) //szybciej sie wykonuje na poziomie procesora niz (int i = 0; i < n; i++)
	{
		cin >> x >> y;
		xxo = x-xo;
		yyo = y - yo;
		p = pow(xxo, 2) + pow(yyo, 2);
		
		if (p == roo) 
		{
			cout << "E" << endl;
		}
		if (p > roo)
		{
			cout << "O" << endl;
		}
		if (p < roo)
		{
			cout << "I" << endl;
		}
	}
	return 0;
}
//PP0602A - Parzyste nieparzyste
//https://pl.spoj.com/problems/PP0602A/

#include <iostream>
using namespace std;

int main() 
{
	int t;
	cin >> t;
	while (t--)
	{
		int n;
		cin >> n;
		int *tab = new int [n];
		for (int i = 0; i < n; i++)
		{
			cin >> tab[i];
		}
		for (int i = 1; i < n; i = i+2)
		{
			cout << tab[i] << " ";
		}
		for (int i = 0; i < n; i = i+2)
		{
			cout << tab[i] << " ";
		}
	}
	return 0;
}
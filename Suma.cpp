//SUMA - Suma
//https://pl.spoj.com/problems/SUMA/

#include <iostream>
using namespace std;

int liczba;

int main() 
{
	int wynik = 0;
	while (cin >> liczba)
	{
		wynik = wynik + liczba;
		cout << wynik << '\n';
	}
	return 0;
}
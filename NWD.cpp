//PP0501A - NWD
//https://pl.spoj.com/problems/PP0501A/

#include <iostream>
//#include <cstdlib>
using namespace std;

int t;
int a, b;

int NWD(int a, int b)
{
    while(a != b)
    {
       if(a > b)
       {
           a-=b; //a = a - b
       }
       else if (b > a)
       {
           b-=a; //b = b - a
       }
    }
    return a; //lub b
}

int main()
{
	cin >> t;
	if (t > 0)
	{
		for (int i = 0; i < t; i++)
		{
			cin >> a >> b;
			if (0 <= a,b <= 1000000)
			{
				int wynik;
				wynik = NWD(a,b);
				cout << wynik << endl;
			}
		}
	}
	//system("PAUSE");
	return 0;
}
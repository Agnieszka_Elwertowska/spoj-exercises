//PA05_POT - Czy umiesz potęgować
//https://pl.spoj.com/problems/PA05_POT/
//Fajne wytłumaczenie zadania - https://www.youtube.com/watch?v=quIWMV3XdxU&list=PLiG7bZwihoSh6hg4KZvaExI94Ffc0M8G_&index=4

#include <iostream>
using namespace std;

int ostatnia_cyfra[10][4] =
	{
		{0,0,0,0}, //ostatnia cyfra potęgowania liczby z końcówką 0
		{1,1,1,1}, //ostatnia cyfra potęgowania liczby z końcówką 1
		{6,2,4,8}, //ostatnia cyfra potęgowania liczby z końcówką 2
		{1,3,9,7}, //ostatnia cyfra potęgowania liczby z końcówką 3
		{6,4,6,4}, //ostatnia cyfra potęgowania liczby z końcówką 4
		{5,5,5,5}, //ostatnia cyfra potęgowania liczby z końcówką 5
		{6,6,6,6}, //ostatnia cyfra potęgowania liczby z końcówką 6
		{1,7,9,3}, //ostatnia cyfra potęgowania liczby z końcówką 7
		{6,8,4,2}, //ostatnia cyfra potęgowania liczby z końcówką 8
		{1,9,1,9}  //ostatnia cyfra potęgowania liczby z końcówką 9
	};

int main() 
{
	int d;
	cin >> d;
	if (1 <= d <= 10)
	{
		while (d--)
		{
			int a, b;
			cin >> a >> b; //a - podstawa, b - wykładnik
			if (1 <= a,b <= 1000000000)
			{
				cout << ostatnia_cyfra[a % 10][b % 4] << '\n';
			}
		}
	}
	return 0;
}
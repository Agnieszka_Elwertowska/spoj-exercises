//TRN - Transponowanie macierzy
//https://pl.spoj.com/problems/TRN/

//Ta wersja kodu działa dla SPOJa, ale nie powinno się deklarować tablicy w ten sposób

#include <iostream>
using namespace std;

int wiersz, kolumna;

int main()
{
	cin >> wiersz >> kolumna;
	int macierz[wiersz][kolumna]; //to nie działa w visual studio

	for (int i = 0; i < wiersz; i++)
	{
		for (int j = 0; j < kolumna; j++)
		{
			cin >> macierz[i][j];
		}
	}
	for (int i = 0; i < kolumna; i++)
	{
		for (int j = 0; j < wiersz; j++)
		{
			cout << " " << macierz[j][i];
		}
	}
	return 0;
}
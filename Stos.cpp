//STOS - Stos
//https://pl.spoj.com/problems/STOS/

#include <iostream>
using namespace std;

int stos[10];
char znak;
int wielkoscstosu = 0;

void poloz()
{
	if (wielkoscstosu>=10)
	{
		cout << ":(" << endl;
	}
	else
	{
		cin >> stos[wielkoscstosu];
		wielkoscstosu++;
		cout << ":)" << endl;
	}
}

void zdejmij()
{
	if (wielkoscstosu<1)
	{
		cout << ":(" << endl;
	}
	else
	{
		cout << stos[wielkoscstosu-1] << endl; //bo w aktualnej pozycji nie ma wartosci
		wielkoscstosu--;
	}
}

int main()
{
	while (cin >> znak)
	{
		if (znak == '+')
		{
			poloz();
		}
		else if (znak == '-')
		{
			zdejmij();
		}
	}

	return 0;
}
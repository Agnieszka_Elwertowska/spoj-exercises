//CALC2 - Kalkulator 2
//https://pl.spoj.com/problems/CALC2/

#include <iostream>
using namespace std;

int rejestr[10];
char znak;
int a, b;

int main() 
{
	while (cin >> znak >> a >> b)
	{
		switch (znak)
		{
			case 'z':
				rejestr[a] = b;
				break;
			case '+':
				cout << (rejestr[a] + rejestr[b]) << '\n';
				break;
			case '-':
				cout << (rejestr[a] - rejestr[b]) << '\n';
				break;
			case '*':
				cout << (rejestr[a] * rejestr[b]) << '\n';
				break;
			case '/':
				cout << (rejestr[a] / rejestr[b]) << '\n';
				break;
			case '%':
				cout << (rejestr[a] % rejestr[b]) << '\n';
				break;
		}
	}
	return 0;
}
//POL - Połowa
//https://pl.spoj.com/problems/POL/

#include <iostream>
#include <string>
using namespace std;

int main() 
{
	int t;
	cin >> t;
	if (1 <= t <= 100)
	{
		while (t--)
		{
			string wyraz;
			cin >> wyraz;
			int p = wyraz.length()/2;
			for (int i = 0; i < p; i++)
			{
				cout << wyraz[i];
			}
			cout << '\n';
		}
	}
	return 0;
}
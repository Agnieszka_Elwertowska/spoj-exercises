//GLUTTON - Obżartuchy
//https://pl.spoj.com/problems/GLUTTON/

#include <iostream>
using namespace std;

int t;
int o, cwp;
int s;
int pudelka;

int main() 
{
	cin >> t;
	while (t--)
	{
		cin >> o >> cwp;
		if (1 <= o <= 10000 && 1 <= cwp <= 1000000000)
		{
			int w = 0;
			while (o--)
			{
				cin >> s;
				w = w + (86400 / s);
			}
			
			pudelka = w / cwp;
			
			if (w % cwp != 0)
			{
				cout << (pudelka + 1) << '\n';
			}
			else
			{
				cout << pudelka << '\n';
			}
		}
	}
	return 0;
}
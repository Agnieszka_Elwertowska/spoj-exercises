//PP0602D - ROL (k)
//https://pl.spoj.com/problems/PP0602D/

#include <iostream>
using namespace std;

int main() 
{
	int n, k;
	cin >> n >> k;
	if ((1 <= k <= 10000) && (1 <= n <= 10000) && (n > k))
	{
		int * tab = new int[n];
		for (int i = 0; i < n; i++)
		{
			cin >> tab[i];
		}
		for (int i = k; i < n; i++)
		{
			cout << tab[i] << ' ';
		}
		for (int i = 0; i < k; i++)
		{
			cout << tab[i] << ' ';
		}
	}
	return 0;
}
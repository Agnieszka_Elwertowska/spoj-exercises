//JWSPLIN - Współliniowość punktów
//https://pl.spoj.com/problems/JWSPLIN/

#include <iostream>
using namespace std;

int main()
{
	float ax, ay, bx, by, cx, cy;
	float w1, w2, w3, ww;
	int ile;
	cin >> ile;
	for (int i = 0; i < ile; i++)
		{
		cin >> ax >> ay >> bx >> by >> cx >> cy;
		w1 = ax * (by - cy);
		w2 = bx * (cy - ay);
		w3 = cx * (ay - by);
		ww = w1 + w2 + w3;
		if (ww == 0)
			{
				cout << "TAK" << endl;
			}
		else 
			{
				cout << "NIE" << endl;
			}
		}
	return 0;
}
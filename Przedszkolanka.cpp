//PRZEDSZK - Przedszkolanka
//https://pl.spoj.com/problems/PRZEDSZK/

#include <iostream>
//#include <cstdlib>
using namespace std;

int N;
int a, b;

int NWD(int a, int b)
{
    while(a != b)
    {
       if(a > b)
       {
           a-=b; //a = a - b
       }
       else if (b > a)
       {
           b-=a; //b = b - a
       }
    }
    return a; //lub b
}

int main()
{
	cin >> N;
	if (1 <= N <= 20)
	{
		for (int i = 0; i < N; i++)
		{
			cin >> a >> b;
			if (a >= 10 && b <= 30)
			{
				int iloczyn, wynik;
				iloczyn = a*b;
				wynik = iloczyn / NWD(a,b);
				cout << wynik << endl;
			}
		}
	}
	//system("PAUSE");
	return 0;
}
//ETI06F1 - Pole pewnego koła
//https://pl.spoj.com/problems/ETI06F1/

#include <iostream>
#include <cmath>
using namespace std;

double r, d;
double PI = 3.141592654;
double pole;

int main() 
{
	cin >> r >> d; 
	pole = (pow(r,2) - pow(d,2)/4.0) * PI;
	cout << pole << '\n';
	return 0;
}
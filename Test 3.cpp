//PP0601A2 - Test 3
//https://pl.spoj.com/problems/PP0601A2/

#include <iostream>
using namespace std;

int main() 
{
	int a, b, pkt;
	cin >> a;
	cout << a << '\n';
	pkt = 0;
	while (pkt < 3)
	{
		cin >> b;
		cout << b << '\n';
		if (b == 42 && a != 42)
		{
			pkt++;
		}
		a = b;
	}
    return 0;
}